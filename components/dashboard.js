import pkg from 'terminal-kit'
import chalk from 'chalk'
import ping from 'ping'
import https from 'https'

let { terminal } = pkg

let _numBucket = terminal.width - 26

let _aHost = [
  {
    'name': 'DEF_GW',
    'addr': '192.168.0.1',
    'isAlive': null,
    'rtt': []
  },
  {
    'name': 'VPN Soabit',
    'addr': '10.13.1.205',
    'isAlive': null,
    'rtt': []
  },
  {
    'name': 'OrangePI',
    'addr': '192.168.0.2',
    'isAlive': null,
    'rtt': []
  },
  {
    'name': 'INTERNET',
    'addr': '151.7.86.183',
    'isAlive': null,
    'rtt': []
  }
]

let _wanIP = null
let _wanIPURL = "https://www.soabit.com/cgi/ip.cgi"

let globalCounter = 0

function _getColor(t) {
  if (t < 0) return chalk.rgb(255, 0, 0)('-')
  if (t < 5) return chalk.rgb(0, 255, 0)(1)
  if (t < 10) return chalk.rgb(10, 255, 0)(2)
  if (t < 20) return chalk.rgb(45, 255, 0)(2)
  if (t < 30) return chalk.rgb(80, 255, 0)(3)
  if (t < 50) return chalk.rgb(115, 255, 0)(3)
  if (t < 70) return chalk.rgb(150, 255, 0)(4)
  if (t < 90) return chalk.rgb(185, 255, 0)(4)
  if (t < 120) return chalk.rgb(220, 255, 0)(5)
  if (t < 150) return chalk.rgb(255, 255, 0)(5)
  if (t < 180) return chalk.rgb(255, 220, 0)(6)
  if (t < 210) return chalk.rgb(255, 185, 0)(6)
  if (t < 300) return chalk.rgb(255, 150, 0)(7)
  if (t < 400) return chalk.rgb(255, 115, 0)(7)
  if (t < 500) return chalk.rgb(255, 80, 0)(8)
  if (t < 600) return chalk.rgb(255, 45, 0)(8)
  if (t < 999) return chalk.rgb(255, 45, 0)(9)
}

export default {
  
  init: () => {
    terminal.clear()
    terminal.moveTo(1, 1)
    terminal('[' + globalCounter + '] WAN IP [' + _wanIP + ']') 
    
    for (let i = 0 ; i < _aHost.length; i++) {
      // _log(i)
      let m = i + 1
      m += ' - ' + _aHost[i].name
      terminal.moveTo(1, i+2)
      terminal(m)
      terminal.eraseLineAfter()
      for (let j = 0; j < _numBucket; j++) {
        _aHost[i].rtt.push(-1)
      }
    }
  },

  run: () => setInterval( () => 
    {
      // Fire PINGs, fetch results and store them properly
      globalCounter++
      for (let i = 0 ; i < _aHost.length; i++) {
        ping.promise.probe(_aHost[i].addr, {
          timeout: 2,
          min_reply: 1
        }).then( (res) => {
          _aHost[i].isAlive = res.alive
          _aHost[i].rtt.shift()
          _aHost[i].rtt.push(res.time)
        }).catch( (e) => {
          _aHost[i].isAlive = false
          _aHost[i].rtt.shift()
          _aHost[i].rtt.push(-1)
        })
      }

    }, 2000),

  update: () => setInterval( () => 
    {
      terminal.moveTo(1, 1)
      terminal('[' + globalCounter + '] WAN IP [' + _wanIP + ']') 
      terminal.eraseLineAfter()

      for (let i = 0 ; i < _aHost.length; i++) {
        let _status = _aHost[i].isAlive ? chalk.green('*') : chalk.red('-')
        terminal.moveTo(20, i+2)
        terminal('[' + _status + ']')

        terminal.moveTo(24, i+2)
        for (let j=0 ; j < _numBucket; j++) {
          let color = _getColor(_aHost[i].rtt[j])
          terminal(color ? color : chalk.rgb(255, 0, 0)('-'))
        }
        terminal.eraseLineAfter()
      }
    }, 2000),

  refreshIP: () => setInterval( () => 
    {
      let req = https.get(_wanIPURL, (res) => {
        res.on('data', (d) => {
          _wanIP = chalk.green.bold(d)
        })
      })
      req.on('error', (e) => {
        _wanIP = chalk.red.bold('**OFFLINE**')
      });
    }, 5000)
  }