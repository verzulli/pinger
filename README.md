# PINGER
#### A tool to real-time, concurrently check the reachibility of multiple IP destinations
#### v. 0.1 - 27/12/2021 - Released under the GPL v2 License (see enclosed LICENSE.txt)

-----------
![](assets/pinger_video.gif)
## 1 - What's this?

    * Junior (shouting): "Daddy! Wifi is not working anymore!!!!"
    * Me: "Let me check. Hold on!"
    
...some time after:

    * Me: "Dear, problem is [our wifi; our router; our ISP; the server you're trying to access]"

This tool is something purposedly developed to shorten the troubleshooting time, between point 2 and 3.

## 2 - Ok. Got it! But... what's _exactly_?

PINGer is an application that every **N** seconds (currently, 2), sends several *PING* toward selected destinations, and report results on a sliding window, constantly refreshed.

By doing this, it gives you a clear figure about your Internet connectivity, on a 2-seconds granularity over a timeframe ranging from ~ 1 minutes (if used in a windows as large as 30 columns) or ~10 minutes (when used with tiny fonts on a large window)

It has been inspider by [Poing](http://poe.perl.org/?Poing), that was developed in PERL. As I needed to enhance some of its feature, I decided to rewrite it in NodeJS. Indeed, I was tempted to develop a plasma widget but... that is definitely beyond my skill. So.. here is the NodeJS version, TUI-based.

## 3 - Requirements
To run this software (at this stage of development) you only need a Linux system being able to show a console.

To customize targets (...at this stage of development) you need to edit "components/dashboard.js" (I know... I know... it need to be customizable, externally. I know....)

To customize targets.... after editing _dashboard.js_, you need to rebuild the application. Supposing you already have a working [NodeJS](https://nodejs.org/en/) setup, you simply need to _clone_ this repo and:

`npm install`

...update dashboard.js... 

`npm start`, to quickly check if it's working, and when you're ready:

`npm run build`

This will create a single `[...]/build/main.js` that you can launch with `cd build ; node main.js`


## 4 - What if I need further help?

Feel free to get in touch with me directly, with whatever technology you prefer. BTW: I'm on Telegram (@Damiano_Verzulli)

## 5 - My TODO list

* add a proper `.env` file to define every custom parameters (frequency and targets, at first);
* find the best approach to distribute a single binary file, to be executed without relying on a pre-installed NodeJS environment. I already did several experiments with [pkg](https://github.com/vercel/pkg) and [nexe](https://github.com/nexe/nexe), but I failed!
* properly handle containing-window resize
* add an explicitely alert (sound!) when a change in external IP is detected (and yes! I'm using a 4G-wan link, so it... changes! Often!)
* every other things could be useful for me... and for you!