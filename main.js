import dotenv from 'dotenv'
import dashboard from './components/dashboard.js'

dotenv.config({ path: '.env' })

async function run() {
  dashboard.init()
  dashboard.run()
  dashboard.update()
  dashboard.refreshIP()
}

run()
